from flask import Flask, render_template
from flask import request, abort
import os
import codecs

app = Flask(__name__)

"""
original given by instructors
commented out to avoid errors with new program

@app.route("/")
def hello():
    return "UOCIS docker demo 2!"


"""
#calls forbidden_error if .., //, or ~ are found
#calls nout_found if not in templates dir or static dir
#path use from import os

@app.route('/<path:path>')
def lookforpath(path):
    if("~" in path or ".." in path or "//" in path):
       return forbidden_error(403) 
    if (os.path.exists('./templates/'+path)):
       return render_template("trivia.html")
    if (os.path.exists('./static/'+path)):
       return render_template("trivia.html")
    else:
       return not_found(404)

#throws error 403 when called
@app.errorhandler(403)
def forbidden_error(e):
    return render_template("403.html"), 403

#throws error 404 when called
@app.errorhandler(404)
def not_found(e):
       return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,port=5000)

